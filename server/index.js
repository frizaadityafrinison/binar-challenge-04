// console.log("Implement servermu disini yak 😝!");
'use strict'

// LOAD THE CORE MODULE
const http = require('http');
const fs = require('fs');
const path = require('path');
const url = require('url');

const port = 8000;
const host = 'localhost';
const PUBLIC_DIRECTORY = path.join(__dirname, '..', 'public');

http.createServer((req, res) => {
    let requestURL = req.url;
    switch (requestURL) {
        case "/" || "":
            requestURL = "/index.html";
            break;
        case "/cars" || "carimobil":
            requestURL = "/carimobil.html";
            break;
        default:
            requestURL = req.url;
            break;
    }
    
    // GET THE PATH AND EXTENSION OF FILE
    const parseURL = url.parse(requestURL);
    const pathName = `${parseURL.pathname}`
    const extensionFile = path.parse(pathName).ext;
    const absolutePath = path.join(PUBLIC_DIRECTORY, pathName);

    // SET THE CONTENT TYPE BASED ON FILE
    const mapContent = {
        ".css": "text/css",
        ".jpg": "image/jpeg",
        ".html": "text/html",
        ".js": "text/javascript"
    }

    // READ THE FILE WHETHER IT EXIST OR NOT
    fs.readFile(absolutePath, (err, data) => {
        if(err) {
            res.statusCode = 500;
            res.end("FILE NOT FOUND!");
            console.log(err);
        } else {        
            res.setHeader('Content-Type', mapContent[extensionFile] || "text/plain");
            res.end(data);
        }
    });
})

.listen(port, () => {
    console.log(`http://${host}:${port}`);
})

