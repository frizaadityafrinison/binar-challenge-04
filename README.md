# Binar Car Rental (Chapter 4 - Main Challenge)

A simple website where you can search bunch of cool car that you can rent.

## Description
This project is a continued version from previous repository where i created a simple responsive landing page using Bootstrap.
This version however has already implemented with HTTP Server using Node.js and Document manipulating or DOM (Document Object Model) using Javascript.
Consequently make this version was no longger a static page but dynamic one, you can search bunch of cool car from HTML form and expect some result from it.




## Tech Stack
Here are the languages and libraries that i used and its version:

**Client:** HTML 5, CSS, Bootstrap 5, Javascript

**Server:** Node.js 16


## Installation

Make sure npm package manager has already installed on your computer

```bash
  cd /your-desired-directory
  git clone https://gitlab.com/frizaadityafrinison/binar-challenge-04
```
    
## Usage
After you clone this repository go to its root directory and run this command to install its dependencies.
```
npm install
```
Once the dependencies are installed, you can write this command to start the application.
```
npm run start
```
To see the website first you need to insert this link into your browser address (Make sure you are running the latest version of your browser)
```
http://localhost:8000
```
Once the webpages loaded, to search for car you can click "Mulai Sewa Mobil" button, you will be redirected into another webpage and from there you can search a bunch of cool car based on data you input.


## Screenshots

![_A30518E0-6138-4443-8A45-A14EA089D5ED_](/uploads/321ff7701431f09fd66aeabd26f6da78/_A30518E0-6138-4443-8A45-A14EA089D5ED_.png)
![_23901589-75C3-4424-9E50-449ADB81AD65_](/uploads/c1a071e0005bbf51165e7283487a33ca/_23901589-75C3-4424-9E50-449ADB81AD65_.png)


## License

This project is licensed under the [MIT](https://choosealicense.com/licenses/mit/) license.


## Authors

- [Friza Aditya Frinison](https://gitlab.com/frizaadityafrinison)
- [Fikri Rahmat Nurhidayat](https://github.com/fnurhidayat)

